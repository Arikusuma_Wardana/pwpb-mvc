<?php 
require_once ('../app/core/Controller.php');

    class User extends Controller 
    {
        public function __construct( )
        {
            if (!empty($_SESSION['id'])) {
                header('Location: ' . BASE_URL . '/home'); 
            }
        }

        public function create() {  
            $data['title'] = 'Register';
            $data['pass_err'] = '';

            $this->view("register/index", $data);
        }

        public function add() {

            if($_POST['password'] == $_POST['verify']) {
                if( $this->model('UserModel')->createUser($_POST) > 0 ) {
                    Flasher::setFlash('Register', 'Berhasil', 'success');
                    header('Location: ' . BASE_URL . '/login/index');
                    exit;
                } 
            } else {
                $data['title'] = 'Register';
                $data['pass_err'] = 'Password Must Be Match';
                Flasher::setFlash('Password', 'Must Be Match', 'danger');
                $this->view("register/index", $data);
            }
        }

        
    }

?>