<?php 
    require_once ('../app/core/Controller.php');

    class Login extends Controller
    {
        public function __construct()
        {
            if (!empty($_SESSION['id'])) {
                    header('Location: ' . BASE_URL . '/home'); 
            }
        }

        public function index() {
            
            $data['title'] = 'Login';
            $data['message'] = '';

            $this->view("login/index", $data);
        }

        public function userLogin() {
            // var_dump($_POST['email']);
            // die;
            if($this->model('Auth')->findUserByEmail($_POST['email'])) {
                $loginInUser = $this->model('Auth')->login($_POST);
                if($loginInUser) {
                    $data['username'] = $_SESSION['username'];
                    ($this->model('Auth')->session($loginInUser));
                    Flasher::setFlash('Login', 'Berhasil', 'success');
                } else {
                    $data['title'] = 'Login';
                    $data['message'] = 'Email Or Password Incorect';
                    Flasher::setFlash('Login', 'Gagal', 'danger');
                    $this->view('login/index', $data);
                }
            } else {
                $data['title'] = 'Login';
                $data['message'] = 'Email Not Found';
                Flasher::setFlash('Login', 'gagal', 'danger');
                $this->view('login/index', $data);
            }
        }

        public function logout() {
            $_SESSION = [];
            Flasher::setFlash('Logout', 'success', 'success');
            header('Location: ' . BASE_URL . '/login/index');
        }
    }


?>