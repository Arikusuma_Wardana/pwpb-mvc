<?php 
require_once ('../app/core/Controller.php');

    class Home extends Controller
    {
        public function __construct()
        {
            if (empty($_SESSION['id'])) {
                 header('Location: ' . BASE_URL . '/login'); 
            }
        }

        public function index() {
            $data['title'] = 'Home'; 
            $data['nama'] = $this->model('UserModel')->getUser();
            $data['user'] = $this->model('UserModel')->getAllUser();

            $this->view("templates/header", $data);
            $this->view("home/index", $data);
            $this->view("templates/footer", $data);
        }

        public function about($company = 'SMKN1') {
            $data['title'] = 'About';
            $data['company'] = $company;

            $this->view("templates/header", $data);
            $this->view("home/index", $data);
            $this->view("templates/footer", $data);
        }

        public function add() {
            if( $this->model('UserModel')->createUser($_POST) > 0 ) {
                header('Location: ' . BASE_URL . '/home/index');
                exit;
            } 
        }
        
    }

?>