<?php 

Class Blog extends Controller {
    public function index()
    {
        $data['title'] = 'Blog';

        $this->view("templates/header", $data);
        $this->view("post/index", $data);
        $this->view("templates/footer", $data);
    }
}


?>