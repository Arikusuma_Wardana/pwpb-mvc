
<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group me-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
          </div>
          <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <span data-feather="calendar" class="align-text-bottom"></span>
            This week
          </button>
        </div>
      </div>

      <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>

      <!-- <h2>Selamat Datang <?= !isset($data['company']) ?: $data['company'] ?></h2> -->
      <!-- <h2>Selamat Datang, <?= $data['nama'] ?></h2> -->

      <?php Flasher::flash(); ?>
      <!-- <?= "<h1> Welcome, ".$_SESSION['first_name']. " " .$_SESSION['last_name']."</h1>"; ?> -->
      <?php 
      if (isset($data['username'])) {
        echo "<h1>" . $data['username'] . "</h1>";
      } elseif (isset($data['company'])) {
        echo "<h1>" .$data['company'] . "</h1>";
      }
      
      ?>

      <!-- Modal -->
      <div class="modal fade" id="info" tabindex="-1" aria-labelledby="infoLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h1 class="modal-title fs-5" id="exampleModalLabel">Detail User</h1>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <?= "<b><p> Name</b> :  ".$_SESSION['first_name']." ".$_SESSION['last_name']."</p>" ?>
              <?= "<b><p> Username</b> :  ".$_SESSION['username']."</p>" ?>
              <?= "<b><p> Email</b> :  ".$_SESSION['email']."</p>" ?>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      
      
      <div class="d-flex justify-content-between">
        <!-- Button Detail Info trigger modal -->
        <div class="ms-2 my-4">
          <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#info">
            Detail Info
          </button>
        </div>
        <!-- Button trigger modal -->
          <div class="ms-2 my-4 me-5">
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
              Create User
            </button>
          </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Create User</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= BASE_URL; ?>/home/add" method="post">
              <div class="modal-body">
                <div class="mb-3">
                  <label class="form-label">Username</label>
                  <input type="text" name="username" class="form-control">
                </div>
                <div class="mb-3">
                  <label class="form-label">Email</label>
                  <input type="email" name="email" class="form-control">
                </div>
                <div class="mb-3">
                  <label class="form-label">First Name</label>
                  <input type="text" name="first" class="form-control">
                </div>
                <div class="mb-3">
                  <label class="form-label">Last Name</label>
                  <input type="text" name="last" class="form-control">
                </div>
                <div class="mb-3">
                  <label class="form-label">Password</label>
                  <input type="password" name="password" class="form-control">
                  <div class="form-text">We'll never share your password with anyone else.</div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Create</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="table-responsive">
        
        <table class="table table-striped table-sm">
          <thead>
            <tr class="text-center">
              <th scope="col">ID</th>
              <th scope="col">Username</th>
              <th scope="col">Email</th>
              <th scope="col">First Name</th>
              <th scope="col">Last Name</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($data['user'] as $item ) : ?>
              <tr class="text-center">
                <td><?= $item['id'] ?></td>
                <td><?= $item['username'] ?></td>
                <td><?= $item['email'] ?></td>
                <td><?= $item['first_name'] ?></td>
                <td><?= $item['last_name'] ?></td>
                
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </main>