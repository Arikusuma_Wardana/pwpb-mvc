<?php 
class Flasher
{
    public static function setFlash($pesan, $aksi, $tipe) {
        $_SESSION['flash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function flash() {
        if (isset($_SESSION['flash'])) {
            echo '
            <div class="alert alert-' . $_SESSION['flash']['tipe'] . ' alert-dismissable fade show" role="alert">
                <strong> '.$_SESSION['flash']['pesan'].' </strong> '. $_SESSION['flash']['aksi'] .'
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>';
            unset($_SESSION['flash']);
        }
    }
}

// Contoh
// public function tambah() {
//     if($this->model('Blog_model')->niatArtikel($_POST) > 0) {
//         Flasher::setFlash('berhasil', 'ditambahkan', 'success');
//         header('Location: ' . BASE_URL . '/blog');
//         exit;
//     }
//      else {
//         Flasher::setFlash('gagal', 'ditambahkan', 'danger');
//         header('Location: ' . BASE_URL . '/blog');
//         exit;
//      }
// }

// public function tambah() {
//     if($this->model('Blog_model')->niatArtikel($_POST) > 0) {
//         Flasher::setFlash('berhasil', 'ditambahkan', 'success');
//         header('Location: ' . BASE_URL . '/blog');
//         exit;
//     }
//      else {
//         Flasher::setFlash('gagal', 'ditambahkan', 'danger');
//         redirect('home/index')
//      }
// }

// function redirect($string) {
//     header('Location: ' . BASE_URL . '/' . $string);
//     exit;
// }
?>

<!-- <div class="row">
    <div class="col-lg-6">
        
    </div>
</div> -->

