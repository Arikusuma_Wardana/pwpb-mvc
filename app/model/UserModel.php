<?php 
class UserModel {
    private $table = 'user';
    private $db;

    private $name = 'Manuk Lebam';
    public function getUser() {
        return $this->name;
    }

    public function __construct() 
    {
        $this->db = new Database;
    }
    // public function getAllUser() {
    //     $this->db->query("SELECT * FROM {$this->table}");
    //     return $this->db->resultAll();
    // }
    public function getAllUser() {
        $this->db->query("SELECT * FROM phpmvc.{$this->table} ORDER BY id DESC");
        return $this->db->resultAll();
    }

    public function getUserById($id) {
        $this->db->query("SELECT * FROM phpmvc.{$this->table} WHERE id=:id");
        $this->db->bind('id', $id);
        return $this->db->resultSingle(); 
    }

    public function createUser($data) {
        $password = password_hash($data['password'], PASSWORD_BCRYPT);
        
        $query = "INSERT INTO phpmvc.user (username, email, first_name, last_name, password) VALUES (:username, :email, :first_name, :last_name, :password)";

        $this->db->query($query);
        $this->db->bind('username', htmlspecialchars($data['username']));
        $this->db->bind('email', htmlspecialchars($data['email']));
        $this->db->bind('first_name', htmlspecialchars($data['first']));
        $this->db->bind('last_name', htmlspecialchars($data['last']));
        $this->db->bind('password', $password);

        $this->db->execute();

        return $this->db->rowCount();
    }
}

?>