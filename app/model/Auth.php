<?php 
class Auth {
    private $table = 'user';
    private $db;

    public function __construct() 
    {
        $this->db = new Database;
    }
    
    public function findUserByEmail($email) {
        $this->db->query("SELECT * FROM phpmvc.{$this->table} WHERE `email` = :email ");
        $this->db->bind('email', $email);
        $row = $this->db->resultSingle();

        if($this->db->rowCount() > 0 ) {
            return $row;
        } else {
            return false;
        }
    }

    public function login($data) {
        $userEmail = htmlspecialchars($data['email']);
        $row = $this->findUserByEmail($userEmail);
        if( $row == false ) return false;
        $hashedPass = $row['password'];
        
        if(password_verify($data['password'], $hashedPass)) {
            return $row;
        } else {
            return false;
        }
    }

    public function session($user) {
        $_SESSION['id'] = $user['id'];
        $_SESSION['username'] = $user['username'];
        $_SESSION['first_name'] = $user['first_name'];
        $_SESSION['last_name'] = $user['last_name'];
        $_SESSION['email'] = $user['email'];

        header('Location: ' . BASE_URL . '/home/index');
    }

    
}

?>